//
//  ViewController.swift
//  appQUOTE
//
//  Created by Zibra Coders on 3/22/19.
//  Copyright © 2019 Zibra Coders. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var quotes = [String]()
    @IBOutlet weak var lblQuote: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        lblQuote.text = ""
        for i in 0..<20 {
            quotes.append("http://www.quoteland.com/author/Oscar-Wilde-Quotes/ \(i)")
        }
        
    }

    @IBAction func btnView(_ sender: Any) {
        let index = Int.random(in: 1 ..< 20)
        lblQuote.text = quotes[index - 1]
        
    }
    
}

